@extends('layouts.app')

@section('content')

    @if(Session::has('error'))
    <div class="alert alert-danger" role="alert">
        {{ Session::get('error') }}
    </div>
    @endif
    <h1>All Products</h1>
    <ul class="list-group  mb-4">
    @forelse(App\Entity\Product::all() as $product)
        <li class="list-group-item product">
            <a href="{{ url('/products/' . $product->id) }}">{{ $product->name }}</a>
            {{ $product->user->name }}
        </li>
    @empty
    <li class="list-group-item">no products found</li>
    @endforelse
    </ul>
    <a class="btn btn-primary" href="{{ url('/products/create') }}">Add</a>

@endsection