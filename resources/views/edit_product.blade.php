@extends('layouts.app')

@section('content')

<form method="POST" class="form-inline" action="{{ url('/products/' . $product->id) }}">
    @csrf
    @method('PATCH')
    <input class="form-control mr-2" type="text" name="name" value="{{ $product->name }}">
    <input class="form-control mr-2" type="text" name="price" value="{{ $product->price }}">
    <button class="btn btn-primary" type="submit">Save Changes</button>
</form>

<div class="mt-4">
    @foreach($errors->all() as $error)
        <p class="text-danger">{{$error}}</p>
    @endforeach
</div>

@endsection