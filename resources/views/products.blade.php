@extends('layouts.app')

@section('content')

    @if(Session::has('error'))
    <div class="alert alert-danger" role="alert">
        {{ Session::get('error') }}
    </div>
    @endif
    <h1>Products</h1>
    <ul class="list-group  mb-4">
    @forelse($products as $product)
        <li class="list-group-item"><a href="{{ url('/products/' . $product->id) }}">{{ $product->name }}</a></li>
    @empty
    <li class="list-group-item">you do not have any products</li>
    @endforelse
    </ul>
    <a class="btn btn-primary" href="{{ url('/products/create') }}">Add</a>

@endsection