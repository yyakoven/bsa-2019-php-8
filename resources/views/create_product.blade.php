@extends('layouts.app')

@section('content')

<form class="form-inline" action="{{ url('products') }}" method="POST">
    @csrf
<input class="form-control mr-2 mb-0" name="name" type="text" placeholder="name" value="{{ old('name') }}">
    <input class="form-control mr-2 mb-0" name="price" type="text" placeholder="price" value="{{ old('price') }}">
    <button class="btn btn-primary" type="submit">Add Product</button>
</form>

<div class="mt-4">
    @foreach($errors->all() as $error)
        <p class="text-danger">{{$error}}</p>
    @endforeach
</div>

@endsection