@extends('layouts.app')
@section('content')
    <p>Name: {{ $product->name }}</p>
    <p>Price: {{ $product->price }}</p>
    <a class="btn btn-primary" href="{{ url('/products/' . $product->id . '/edit') }}">Edit</a>

    <form class="d-inline" method="POST" action="{{ url('/products/' . $product->id) }}">
        @csrf
        @method('DELETE')
        <button class="btn btn-primary" type="submit">Delete</button>
    </form>
@endsection
