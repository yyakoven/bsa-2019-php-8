<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entity\Product;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{

    public function index()
    {
        $user = auth()->user();
        return view('products')->withProducts($user->products);
    }

    public function create()
    {
        return view('create_product');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|min:3',
            'price' => 'numeric|min:0'
        ]);
        $data['user_id'] = auth()->user()->id;
        Product::create($data);
        return redirect('/products');
    }

    public function show(Product $product)
    {
        if(Gate::denies('handle', $product))
        {
            Session::flash('error', 'you are not authorized to view this page');
            return redirect('/products');
        }
        return view('product')->withProduct($product);
    }

    public function edit(Product $product)
    {
        if(Gate::denies('handle', $product))
        {
            Session::flash('error', 'you are not authorized to view this page');
            return redirect('/products');
        }
        return view('edit_product')->withProduct($product);
    }

    public function update(Request $request, Product $product)
    {
        if(Gate::denies('handle', $product))
        {
            Session::flash('error', 'you are not authorized to view this page');
            return redirect('/products');
        }
        $data = $request->validate([
            'name' => 'required|min:3',
            'price' => 'numeric|min:0'
        ]);
        $product->update($data);
        $product->save();
        return redirect('products/' . $product->id);
    }

    public function destroy(Product $product)
    {
        if(Gate::denies('handle', $product))
        {
            Session::flash('error', 'you are not authorized to view this page');
            return redirect('/products');
        }
        $product->delete();
        return redirect('/products');
    }
}
