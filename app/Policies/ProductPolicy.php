<?php

namespace App\Policies;

use App\Entity\User;
use App\Entity\Product;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;
    
    public function before(User $user) {
        if ($user->is_admin)
        {
            return true;
        }
    }

    public function handle(?User $user, Product $product)
    {
        return $product->user_id == $user->id;
    }
}
