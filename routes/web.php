<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();
Route::get('login/github', 'Auth\LoginController@redirectToProvider');
Route::get('auth/github/callback', 'Auth\LoginController@handleProviderCallback');

Route::resource('products', 'ProductController')->middleware('auth');
Route::get('dashboard', function(){
        return view('dashboard');
})->middleware('can:handle,App\Entity\User,App\Entity\Product');
